#!/bin/bash

alias update='sudo apt-get update'
alias upgrade='sudo apt-get update && sudo apt-get upgrade && sudo apt-get dist-upgrade && sudo apt-get autoremove'
alias install='sudo apt-get install'
alias deinstall='sudo apt-get purge $1 && sudo apt-get autoremove'
alias remove='sudo apt-get purge $1 && sudo apt-get autoremove'
alias reboot='sudo reboot'
alias restart='sudo reboot'
alias stop='sudo shutdown now'
alias pi='ssh skankjo@84.240.15.226'
alias is='ps aux | grep'
alias awsssh='ssh glassfish@app.lt.test-01.dev.4finance.net'
alias activemq='/home/skankjo/apps/apache-activemq-5.13.3/bin/activemq start'
alias setkey='gsettings set org.gnome.desktop.input-sources current '
alias mouse='upower -i $(upower -e | grep -E "mouse") | grep -E "state|time\ to\ full|percentage"'
alias battery='upower -i $(upower -e | grep -E "battery") | grep -E "state|time\ to\ full|percentage"'
alias test-pipeline='ssh dmitry.shenk@app.se.test-pipeline.dev.4finance.net'
alias kompaslaugos='open_command https://docs.google.com/document/d/1QEjZSs2c4bOgBEU5su1kaWM9nGM53B_ToN1laQn2OGc/edit'
alias gmail='open_command https://mail.google.com/mail/u/0/#inbox'
alias setjava7='JAVA_HOME=/home/skankjo/apps/jdk1.7.0_67'
alias echojava='echo $JAVA_HOME'
alias lt.app1='terminal-production dci1-alt-001'
alias mx.app1='terminal-production dci2-amx-001'
alias lt.app2='terminal-production dci1-alt-002'
alias lt.web1='terminal-production dci1-wlt-001'
alias lt.web2='terminal-production dci1-wlt-002'
alias ro.app1='terminal-production dci1-aro-001'
alias ro.app2='terminal-production dci1-aro-002'
alias ro.web1='terminal-production dci1-wro-001'
alias ro.web2='terminal-production dci1-wro-002'
alias ro.fro2='terminal-production dci1-fro-002'
alias lv.app3='terminal-production dci1-alv-003'
alias lv.app1='terminal-production dci1-alv-001'
alias lv.web1='terminal-production dci1-wlv-001'
alias dk.app1='terminal-production dci1-adk-001'
alias dk.web1='terminal-production dci1-wdk-001'
alias dk.fro1='terminal-production dci1-fdk-001'
alias dk.fro2='terminal-production dci1-fdk-002'
alias fi.app1='terminal-production dci1-afi-001'
alias pl.app1='terminal-production dci1-apl-001'
alias pl.web1='terminal-production dci1-wpl-001'
alias mx.web1='terminal-production dci1-wmx-001'
alias pl.app2='terminal-production dci1-apl-002'
alias lt.stage='terminal-nonprod dcm1-stg-003'
alias lt.test='ssh dmitry.shenk@app.lt.test-pipeline.dev.4finance.net'
alias lt.stage='ssh dmitry.shenk@app.lt.stage-pipeline.dev.4finance.net'
alias install_deb='sudo dpkg -i '
alias remove_deb='sudo dpkg -r '
alias ro.fe.test='ssh ec2user@frontend-01.ro.test-pipeline.dev.4finance.net -i ~/Sites/4finance-chef/.chef/translation_server-ec2'
alias ro.fe.stage='ssh ec2user@frontend-01.ro.stage-pipeline.dev.4finance.net -i ~/Sites/4finance-chef/.chef/translation_server-ec2'
alias se.web1='terminal-production dci1-wse-001'
alias se.web2='terminal-production dci1-wse-002'
alias ro.test='ssh dmitry.shenk@app.ro.test-pipeline.dev.4finance.net'
alias ro.stage='ssh dmitry.shenk@app.ro.stage-pipeline.dev.4finance.net'
alias dk.test='ssh dmitry.shenk@app.dk.test-pipeline.dev.4finance.net'
alias dk.stage='ssh dmitry.shenk@app.dk.stage-pipeline.dev.4finance.net'
alias dk.fe.stage='ssh dmitry.shenk@192.168.52.254'
alias ge.web1='terminal-production dci1-wge-001'
alias ge.app1='terminal-production dci1-age-001'
alias ge.fro1='terminal-production dci1-fge-001'
alias ge.fro3='terminal-production dci1-fge-003'
alias pl.stage='ssh dmitry.shenk@app.pl.stage-pipeline.dev.4finance.net'
alias ge.stage='ssh dmitry.shenk@app.ge.stage-pipeline.dev.4finance.net'
alias ge.test='ssh dmitry.shenk@app.ge.test-pipeline.dev.4finance.net'
alias apl.pip3.test='ssh dmitry.shenk@app.pl.test-pipeline3.dev.4finance.net'
alias fpl.pip3.test='ssh dmitry.shenk@frontend.pl.test-pipeline3.dev.4finance.net'
alias vpn='nmcli conn up id dmitry.shenk@avpn.4finance.net'
alias stopvpn='nmcli conn down id dmitry.shenk@avpn.4finance.net'
alias stop_vpn='stopvpn'
alias restart_vpn='vpn && stopvpn'
alias o='open_command'
alias rmf='rm -fr'
alias untar='tar -zxvf'
alias tarls='tar -tf' #list contents of tar archive
alias d='date +%Y-%m-%d'
alias dt='date +"%Y-%m-%d %T"'
alias rand='echo $RANDOM'
alias rand10='rnd 10'
alias online='internet_up'
alias port='lsof -i -P -n | rg '
alias restart_display='sudo service lightdm restart'
alias lock='gnome-screensaver-command --lock'
alias ls='exa'
alias la='exa -lahug --time-style=long-iso --group-directories-first'
alias gst='git status --untracked-files=all'
alias installed='dpkg -l | rg'
alias cat='batcat'
alias unbz='tar -xjf'
alias ungz='tar -xzf'
alias googler='googler --noua'
alias copy='xclip -selection c'
alias paste='xclip -selection clipboard -o'
alias weather='curl wttr.in'
alias sleep='systemctl suspend'
alias suspend='systemctl suspend'
alias hibernate='systemctl suspend'
alias tree='br'
alias help='tldr'
alias du='duf'
alias pn='pnpm'

function git-next-commit() {
  git rev-list --ancestry-path $1..HEAD | tail -1
}

function internet_up() {
    ifconfig | ack -A 1 eth1 | ack -o 'inet addr:\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}' | awk -F ':' '{print "wired: " $2}'
    ifconfig | ack -A 1 wlan1 | ack -o 'inet addr:\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}' | awk -F ':' '{print "wireless: " $2}'
}

function rnd() {
  [ -z "$1" ] && echo "Usage: rnd base" && return 0
	echo "$[$RANDOM % $1]"
}

function open() {
  [ -z "$1" ] && echo "Usage: open what" && return 0
	nautilus $1 &
}
alias oh='open .'

function vpnst() {
	nmcli conn show id dmitry.shenk@avpn.4finance.net | grep GENERAL.STATE | awk '{print $2}'
}

function recreate_db() {
        psql -d template1 -f ~/bin/.psql/$1
}

function kill_port() {
    procid=$(port $1 | awk '{print $7}' | awk -F / '{print $1}')
    kill $procid
    echo $procid
}

function wired() {
    [ $1 = "up" ] && option="-a"
    nmcli $option conn $1 id darbo
}

function addToAui() {
  [ -z "$1" ] && [ -z "$2" ] && echo "Usage: addToAui group host" && return 0
  group=$1
  host=$2
  curl -XPUT -H "Content-Type: application/json" -H "Accept: application/json" aui.dev.4finance.net:5000/group/$group -d '{"hosts":["'"$host"'"]}'
}

function node-project {
  git init
  npx licence $(npm get init.licence) -o "$(npm get init.author.name)" > LICENCE
  npx gitignore node
  npm init -y
  git add -A
  git commit -m "Initial commit"
}

dev-ssh() {
  vault ssh -address=https://vault.a05.4finance.net -mode=ca -mount-point=ssh-a06 -role=a06-developers ffapp@$1
}

dev-ssh-work() {
  vault ssh -address=https://vault.a05.4finance.net -mode=ca -mount-point=ssh-a06 -role=a06-developers -private-key-path=~/.ssh/id_rsa_work -public-key-path=~/.ssh/id_rsa_work.pub ffapp@$1
}

dev-ssh-ed() {
  vault ssh -address=https://vault.a05.4finance.net -mode=ca -mount-point=ssh-a06 -role=a06-developers -private-key-path=~/.ssh/id_ed25519 -public-key-path=~/.ssh/id_ed25519.pub ffapp@$1
}

function localhost() {
  # this is an assignment with default value
  port=${1:-'8083'}
  ip addr | rg eth0 | rg inet | awk '{print $2}' | awk -F/ -v port=$port '{print "http://" $1 ":" port}'
}

function nth() {
  local i=${1:-'1'}
  local firstSym=${i:0:1}
  if [ $firstSym -eq '-' ]; then
    local arg="$i"
  else
    local arg="+$i"
  fi
  tail -n $arg | head -n 1
}

function kube_connect() {
  [ -z "$1" ] && echo "Usage: kube_connect app_name" && return 0
  local app_name=$1
  local containers=($(kubectl get pods | rg $app_name | rg -v liquibase | awk '{print $1}'))
  local count=${#containers[@]}
  [ $count -gt 1 ] && echo "Too many containers found: $count" && print_arr $containers && return 0
  local pod_name=$(kubectl get pods | rg $app_name | rg -v liquibase | awk '{print $1}')
  kubectl exec --stdin --tty $pod_name --container $app_name -- /bin/bash
}

function kube_debug() {
  [ -z "$1" ] && echo "Usage: kube_debug app_name [port]" && return 0
  local app_name=$1
  local port=${2:-'8449'}
  local containers=($(kubectl get pods | rg $app_name | rg -v liquibase | awk '{print $1}'))
  local count=${#containers[@]}
  [ $count -gt 1 ] && echo "Too many containers found: $count" && print_arr $containers && return 0
  local pod_name=$(kubectl get pods | rg $app_name | rg -v liquibase | awk '{print $1}')
  kubectl port-forward pods/$pod_name $port:8449
}

function print_arr() {
  for e in "$@"; do
    echo $e
  done
} 

function cleanup_branches() {
  for i in `git branch`; do if [ "$i" != 'master' ] && [ "$i" != '*' ]; then git branch -D $i; fi; done
}
