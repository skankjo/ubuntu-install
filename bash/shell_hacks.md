# How to remove file with names containing special characters
[Linux Shell Tip: Remove files with names that contains spaces, and special characters such as -, —](https://www.linux.com/training-tutorials/linux-shell-tip-remove-files-names-contains-spaces-and-special-characters-such/)
Tip #5: Remove file by an inode number
The -i option to ls displays the index number (inode) of each file:

`ls -li`
Use find command as follows to delete the file if the file has inode number 4063242:

`find . -inum 4063242 -delete`

OR

`find . -inum 4063242 -exec rm -i {} ;`
