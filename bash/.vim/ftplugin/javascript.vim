" vim-jsx-pretty
let g:vim_jsx_pretty_colorful_config = 1

" ale
let b:ale_fixers = {'javascript': ['prettier', 'eslint']}
let g:ale_fix_on_save = 1
