# Steps to make the thing work
1. Clone `nano-infra-spring` project.
2. Jump into top project's directory and run `./gradlew :infra-properties-encryption-tool:jar`.
3. An `infra-properties-encryption-tool.jar` file is created inside `${nano-infra-spring-project-dir}/infra-properties-encryption-tool/build/libs` directory. This `jar` should be symlinked into `~/bin` directory. For instance, you go into a directory, where built jar resides, and run this one: `cp -s $(pwd)/infra-properties-encryption-tool.jar ~/bin/encrypt.jar`. Make sure the `~/bin` directory exists on your machine.
4. The prerequisites to make the script work smoothly are done. There are two possibilities to act further on. Either you run the script from origin directory or you make it runnable from anywhere. To make it runnable from anywhere, you have to put it or to symlink it in one of the directories in the $PATH. To see the list type: `echo $PATH`.
5. Create an `encrypt_key` in your working directory, i.e., from where you plan to run the script, e.g., `touch encrypt_key`.
6. Paste inside the `encrypt_key` file the actual key from `password.4finance.net`. 
7. Now you can run the script: `encrypt creditb-lt.yaml "password|key|passcode"`, for instance. The second option is the list of properties to process. The default is just "password".
8. If you run the script without options, it displays usage.
9. The result of the encryption/decryption goes into a new file with the name "${origin_file_name}_bak". Thus the origin file stays intact.
