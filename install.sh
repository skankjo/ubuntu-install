#!/bin/bash

function print() {
	echo -e "\e[1;31m$1\e[0m\n"
}

print "Updating and upgrading the system"
sudo apt -y update && sudo apt -y upgrade

print "Creating user bin directory:"
mkdir ~/bin

print "Installing missing programs"

for program in $(cat programs); do
	installed="$(which $program)"
	print "The bin path to the $program is: $installed"
	print "If there is no path to the $program found, sudo apt install $program is about to be run"
	test -z "$installed" && sudo apt -y install $program
done

for file in $(ls installments); do
	exists="$(grep $file installed)"
	print "Installing the library: $file"
	echo $file >> installed
	test -z "$exists" && . ./installments/$file
done

for app in $(ls -a bin); do
	cp -rf ./bin/$app ~/bin/
done

for script in $(ls -A bash); do
	cp -f ./bash/$script ~/$script
	source ~/$script
done

source current_shell.sh
echo -e "\nsource .profile" >> $CURRENT_SHELL_RC
