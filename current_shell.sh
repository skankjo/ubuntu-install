#!/bin/bash

CURRENT_SHELL=$(ps -p $$ -o cmd=)
rcfile="${HOME}/.${CURRENT_SHELL}rc"

test -f "$rcfile" && CURRENT_SHELL_RC=$rcfile
