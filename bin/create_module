#!/bin/bash

function usage() {
  echo "Usage: $0 [-r|t|p|d] module_name"
  echo "Options:"
  echo "r    Add resources directory"
  echo "t    Add test directory"
  echo "p    Provide package name, if it differs from the module name"
  echo "d    Domain - prefix of all the packages (required)"
  exit 0
}

function exit_abnormal() {
  usage
  exit 1
}

if [ $# -eq 0 ]; then
  exit_abnormal
fi

args=("$@") #put arguments into an array
module="${args[-1]}"
package=${module/.//}
first_letter=${module:0:1}

current_path=$(pwd)
project=${current_path##*/} # remove long prefix: "evetything till a slash"

if [ $first_letter == "-" ]; then
  echo "Or you have forgotten to provide a module name, or it cannot start with the dash"
  exit_abnormal
fi

while getopts "rtp:d:" option; do # colon means an argument must be added to the option
  case $option in
    p)
      package=$OPTARG
      ;;
    d)
      domain=${OPTARG/.//}
      ;;
    r)
      resources=true
      ;;
    t)
      include_test=true
      ;;
    :) # required argument omitted
      echo "Error: -${OPTARG} requires an argument."
      exit_abnormal
      ;;
    *) # unknown option
      echo "Error: unknown option used."
      exit_abnormal
      ;;
  esac
done

if [ -z "$domain" ]; then
  echo "Error: 'domain' option is necessary."
  exit_abnormal
fi

mkdir -p ${module}/src/main/java/${domain}/${project}/${package}

if [ -n "$resources" ]; then
  mkdir -p ${module}/src/main/resources
fi

if [ -n "$include_test" ]; then
  mkdir -p ${module}/src/test/java/${domain}/${project}/${package}
fi

touch ${module}/build.gradle
echo "dependencies {" >> ${module}/build.gradle
echo "}" >> ${module}/build.gradle

module_info=${module}/src/main/java/module-info.java
touch $module_info 
echo "module $module {" >> $module_info
echo "}" >> $module_info

