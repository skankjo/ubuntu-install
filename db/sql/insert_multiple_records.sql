drop table if exists t ;
create table t
( tid serial primary key,
  i int default 0,
  name text default 'Jack'
) ;
with ins as
  (insert into t (i, name)               -- all the columns except any serial
   values (default, default)
   returning i, name
  )
insert into t 
  (i, name)
select 
  ins.i, ins.name
from 
  ins cross join generate_series(1, 9);  -- one less than you need
table t ;
